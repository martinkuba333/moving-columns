import styled from 'styled-components';

export const RectangleShape = styled.div`
  display: inline-block;
  margin: 15px;
  position: absolute;
  transition: all 0.2s linear;
`;

const topColor = '#f0e9f6';
const triangleSize = '20';

const shaping = {
  width: 0,
  height: 0,
  transform: 'rotateX(40deg)',
  borderLeft: `${triangleSize}px solid transparent`,
  borderRight: `${triangleSize}px solid transparent`,
  zIndex: 150,
};

export const RectangleBottom = styled.div`
  ${shaping};
  border-bottom: ${triangleSize}px solid transparent;
  border-top: ${triangleSize}px solid ${topColor};
  top: -10px;
  position: relative;
`;

export const RectangleTop = styled.div`
  ${shaping};
  border-bottom: ${triangleSize}px solid ${topColor};
  border-top: ${triangleSize}px solid transparent;
  position: relative;
`;

const rectangleSides = {
  display: 'inline-block',
  width: '28px',
  height: '160px',
  zIndex: '49',
  transform: 'rotateY(45deg)',
};

export const RectangleBodyLeft = styled.div`
  position: relative;
  top: -45px;
  left: -4px;
  ${rectangleSides};
  background: linear-gradient(to bottom left, #f0e9f6 20%, #2c163d 80%, transparent 100%);
`;

export const RectangleBodyRight = styled.div`
  position: relative;
  top: -45px;
  left: -13px;
  ${rectangleSides};
  background: linear-gradient(to bottom right, #f0e9f6 20%, #2c163d 80%, transparent 100%);
`;
