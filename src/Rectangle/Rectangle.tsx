import React, { CSSProperties, useEffect, useState } from 'react';
import {
  RectangleTop,
  RectangleBottom,
  RectangleShape,
  RectangleBodyLeft,
  RectangleBodyRight,
} from './Rectangle.styles';
import { CONST } from '../Utils/Const';

export interface RectangleInterface {
  leftPos: number;
  topPos: number;
  index: number; //z-index?
  counter: number;
}

export const Rectangle: React.FC<RectangleInterface> = (props: RectangleInterface) => {
  const [top, setTop] = useState<number>(props.topPos);
  const bestZIndex = CONST.TOTAL_RECTANGLES / 2 + 1 > props.index;

  useEffect(() => {
    setTimeout(() => {
      const newTop = top - CONST.RECTANGLES_STARTING_TOP;
      setTop(newTop);
    }, 150 * props.index);
  }, [props.counter]);

  const getRectangleInlineCss = (): CSSProperties => {
    return {
      top: `${top}px`,
      left: `${props.leftPos}px`,
      zIndex: bestZIndex ? props.index : CONST.TOTAL_RECTANGLES - props.index,
    };
  };

  return (
    <>
      <RectangleShape style={getRectangleInlineCss()}>
        <RectangleTop />
        <RectangleBottom />
        <RectangleBodyLeft />
        <RectangleBodyRight />
      </RectangleShape>
    </>
  );
};
