import React, { ReactElement, useEffect, useState } from 'react';
import { Rectangle } from './Rectangle/Rectangle';
import { Rectangles } from './Rectangles/Rectangles';
import { AppContainer } from './App.styles';

function App(): ReactElement<any, any> {
  return (
    <AppContainer>
      <Rectangles />
    </AppContainer>
  );
}

export default App;
