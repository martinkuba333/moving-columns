import styled from 'styled-components';

const sizes = {
  width: '800px',
  height: '700px',
};

export const Board = styled.div`
  background: #2c163d;
  margin-left: 20%;
  ${sizes}
`;

export const RectangleContainer = styled.div`
  position: absolute;
  margin-left: 370px;
  ${sizes}
`;
